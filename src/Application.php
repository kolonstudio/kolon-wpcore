<?php

namespace Kolon\Core;

use Roots\Bedrock\Autoloader as PluginAutoloader;

class Application
{
    use
        Concerns\RegistersSettings,
        Concerns\ConfiguresMenu,
        Concerns\ConfiguresDashboard;

    public function run()
    {
        $this->registerPrettyErrors();
        $this->registerConfig();

        $this->disallowIndexing();
        $this->includeHelpers();
        $this->loadPlugins();
        $this->configureTheme();
        $this->configureMenu();
        $this->configureDashboard();
    }

    private function loadPlugins()
    {
        if (is_blog_installed() && class_exists(PluginAutoloader::class)) {

            new PluginAutoloader();

            do_action('plugins_loaded');
        }
    }

    private function includeHelpers()
    {
        include_once __DIR__ . '/helpers.php';
    }

    private function registerPrettyErrors()
    {
        if ( defined( 'WP_DEBUG' ) && WP_DEBUG ) {
            $whoops = new \Whoops\Run;
            $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
            $whoops->register();
        }
    }

    private function registerConfig()
    {

        Container::getInstance()
            ->bindIf('config', function () {
                $configPath = get_theme_file_path('../config/');
                $configFiles = [];
                if(file_exists($configPath . '/theme.php')) {
                    $configFiles['theme'] = require $configPath . '/theme.php';
                }

                if(file_exists($configPath . '/view.php')) {
                    $configFiles['view'] = require $configPath . '/view.php';
                }

                if(file_exists($configPath . '/menu.php')) {
                    $configFiles['menu'] = require $configPath . '/menu.php';
                }

                return new Config($configFiles);
            }, true);
    }

    private function disallowIndexing()
    {
        // Disallow indexing of your site on non-production environments.
        if (defined('WP_ENV') && WP_ENV !== 'production' && !is_admin()) {
            add_action('pre_option_blog_public', '__return_zero');
        }
    }
}
