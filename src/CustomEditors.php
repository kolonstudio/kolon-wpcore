<?php

namespace Kolon\Core;

class CustomEditors
{
    public $editors = [];

    public function addEditor(CustomEditor $customEditor)
    {
        if (!array_key_exists($customEditor->name, $this->editors)) {
            $this->editors[$customEditor->name] = $customEditor;
        }

        return $this;
    }


    public function publish()
    {
        add_filter('acf/fields/wysiwyg/toolbars', function () {
            $toolbars = [];

            foreach ($this->editors as $editorName => $editor) {
                $toolbars[$editorName] = [];
                $toolbars[$editorName][1] = $editor->buttons;
            }

            return $toolbars;
        });


        add_filter('tiny_mce_before_init', function ($init) {

            foreach ($this->editors as $editor) {
                foreach ($editor->features as $name => $value) {
                    if ($name === 'formats') {
                        $init[$name] = collect($value)->toJson();
                    } else {
                        $init[$name] = $value;
                    }
                }
            }

            return $init;
        });

        // Remove custom color pallet
        add_filter('tiny_mce_plugins', function ($plugins) {

            foreach ($plugins as $key => $plugin_name) {
                if ('colorpicker' === $plugin_name) {
                    unset($plugins[$key]);
                }
            }

            return $plugins;
        });

        return $this;
    }
}
