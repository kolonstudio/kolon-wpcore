<?php

namespace Kolon\Core;

/**
 * Buttons reference
 * @see https://www.tiny.cloud/docs-3x/reference/buttons/
 */
class CustomEditor
{
    public $name = '';
    public $features = [];
    public $buttons = [];

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function pasteAsText()
    {
        $this->features['paste_as_text'] = true;
        return $this;
    }

    public function addBold()
    {
        $this->buttons[] = 'bold';
        return $this;
    }

    public function addItalic()
    {
        $this->buttons[] = 'italic';
        return $this;
    }

    public function addLink()
    {
        $this->buttons[] = 'link';
        return $this;
    }

    public function addUnderline()
    {
        $this->buttons[] = 'underline';
        return $this;
    }

    public function addBullist()
    {
        $this->buttons[] = 'bullist';
        return $this;
    }

    public function addHr()
    {
        $this->buttons[] = 'hr';
        return $this;
    }

    public function addOutdent()
    {
        $this->buttons[] = 'outdent';
        return $this;
    }

    public function addIndent()
    {
        $this->buttons[] = 'indent';
        return $this;
    }

    public function addTextStyles($styleFormats)
    {
        $this->buttons[] = 'styleselect';
        $this->features['style_formats'] = wp_json_encode($styleFormats);

        return $this;
    }

    public function addFormat($key, $format)
    {
        $this->features['formats'] = [$key => $format];

        return $this;
    }

    public function addTextColors($colors, $rows = 1, $cols = 8)
    {
        $colorMap = collect($colors)->map(function($color, $name) {
            return [str_replace('#', '', $color), $name];
        })->flatten()->join('", "');

        $this->buttons[] = 'forecolor';
        $this->features['textcolor_map'] = '["' . $colorMap . '"]';
        $this->features['textcolor_rows'] = $rows;
        $this->features['textcolor_cols'] = $cols;

        return $this;
    }
}
