<?php

namespace Kolon\Core\Concerns;

use Kolon\Core\CustomEditors;

trait RegistersSettings
{

    use RemoveComments,
        BladeTemplateFiles,
        CanBeHeadless;

    private function configureTheme()
    {
        if (config('theme.headless')) {
            $this->configureHeadless();
        } else {
            $this->configureViews();
        }

        if (!config('theme.comments')) {
            $this->removeComments();
        }

        // Add custom editors
        kolon()->singleton('editors', function () {
            return new CustomEditors();
        });

    }

    private function configureViews()
    {

        $this->useBladeTemplateFiles();

        add_action('after_setup_theme', function() {
            /**
             * Enable plugins to manage the document title
             * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
             */
            add_theme_support('title-tag');
            /**
             * Enable post thumbnails
             * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
             */
            add_theme_support('post-thumbnails');
            /**
             * Enable HTML5 markup support
             * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
             */
            add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);
        }, 20);
    }
}
