<?php

namespace Kolon\Core\Concerns;

trait ConfiguresDashboard
{
    private function configureDashboard()
    {
        add_action( 'wp_dashboard_setup', function () {
            // Remove Welcome panel
            remove_action( 'welcome_panel', 'wp_welcome_panel' );

            // Remove the rest of the dashboard widgets
            remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
            remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
            remove_meta_box( 'health_check_status', 'dashboard', 'normal' );
            remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
            remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');
//            remove_meta_box( 'dashboard_site_health', 'dashboard', 'normal');

        } );
    }
}
