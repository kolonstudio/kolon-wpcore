<?php

namespace Kolon\Core\Concerns;

use Kolon\Core\Blade;
use Kolon\Core\Container;
use Kolon\Core\BladeProvider;

trait BladeTemplateFiles
{

    private function useBladeTemplateFiles()
    {
        /**
         * Template Hierarchy should search for .blade.php files
         */
        collect([
            'index', '404', 'archive', 'author', 'category', 'tag', 'taxonomy', 'date', 'home',
            'frontpage', 'page', 'paged', 'search', 'single', 'singular', 'attachment', 'embed'
        ])->map(function ($type) {
            add_filter("{$type}_template_hierarchy", 'filter_templates');
        });

        /**
         * Render page using Blade
         */
        add_filter('template_include', function ($template) {
            collect(['get_header', 'wp_head'])->each(function ($tag) {
                ob_start();
                do_action($tag);
                $output = ob_get_clean();
                remove_all_actions($tag);
                add_action($tag, function () use ($output) {
                    echo $output;
                });
            });
            $data = collect(get_body_class())->reduce(function ($data, $class) use ($template) {
                return apply_filters("kolon/template/{$class}/data", $data, $template);
            }, []);
            if ($template) {
                echo template($template, $data);
                return get_stylesheet_directory() . '/index.php';
            }
            return $template;
        }, PHP_INT_MAX);

        array_map(
            'add_filter',
            ['theme_file_path', 'theme_file_uri', 'parent_theme_file_path', 'parent_theme_file_uri'],
            array_fill(0, 4, 'dirname')
        );

        // Register the theme blade provider as as singleton
        add_action('after_setup_theme', function () {
            /**
             * Add Blade to Kolon container
             */
            kolon()->singleton('kolon.blade', function (Container $app) {
                $cachePath = config('view.compiled');
                if (!file_exists($cachePath)) {
                    wp_mkdir_p($cachePath);
                }
                (new BladeProvider($app))->register();

                return new Blade($app['view']);
            });
        });

        /**
         * Updates the `$post` variable on each iteration of the loop.
         * Note: updated value is only available for subsequently loaded views, such as partials
         */
        add_action('the_post', function ($post) {
            kolon('blade')->share('post', $post);
        });
    }
}
