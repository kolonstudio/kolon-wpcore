<?php

namespace Kolon\Core\Concerns;

trait CanBeHeadless
{
    private $headless_client_url = '';

    private function configureHeadless()
    {
        $this->headless_client_url = config('theme.headless_frontend_url');
        $this->disableFrontend();
        $this->configureLinks();
    }

    private function is_rest()
    {
        if (
            defined('REST_REQUEST') && REST_REQUEST // (#1)
            || isset($_GET['rest_route']) // (#2)
            && strpos($_GET['rest_route'], '/', 0) === 0
        )
            return true;

        // (#3)
        global $wp_rewrite;
        if ($wp_rewrite === null) $wp_rewrite = new \WP_Rewrite();

        // (#4)
        $rest_url = wp_parse_url(trailingslashit(rest_url()));
        $current_url = wp_parse_url(add_query_arg(array()));
        return strpos($current_url['path'] ?? '/', $rest_url['path'], 0) === 0;
    }

    private function disableFrontend()
    {
        global $wp;

        if (
            !$this->is_rest() &&
            !defined('DOING_CRON') &&
            !defined('REST_REQUEST') &&
            !(function_exists('is_graphql_request') ? is_graphql_request() : false) &&
            !(function_exists('is_graphql_http_request') ? is_graphql_http_request() : false) &&
            !($GLOBALS['pagenow'] === 'wp-login.php') &&
            !is_admin() &&
            (empty($wp->query_vars['rest_oauth1']) &&
                !defined('GRAPHQL_HTTP_REQUEST')
            )
        ) {

            wp_safe_redirect(admin_url() . $wp->request);
            exit;
        }
    }

    private function configureLinks()
    {
        if (class_exists('Permalink_Manager_Class')) {
            // Change WP_HOME for HEADLESS_CLIENT_URL
            array_map(function ($filter) {
                add_filter($filter, function ($link) {
                    if (!defined('WP_HOME') || !$this->headless_client_url) {
                        return $link;
                    }
                    return str_replace(WP_HOME, $this->headless_client_url, $link);
                }, PHP_INT_MAX, 1);
            }, ['permalink_manager_filter_permalink_base', 'post_type_link', 'page_link']);
        } else {
            add_action('admin_notices', function () {
                $class = 'notice notice-error';
                $message = 'Please install <a href="https://permalinkmanager.pro/" target="_blank">Permalink Manager</a> plugin to get the correct frontend urls.';
                printf('<div class="%1$s"><p>%2$s</p></div>', esc_attr($class), $message);
            });
        }
    }
}
