<?php

namespace Kolon\Core\Concerns;

trait ConfiguresMenu
{
    private function configureMenu()
    {
        add_action('admin_menu', function () {
            $menus = [
                # Dashboard
                'index.php' => [
                    'show' => config('menu.dashboard', false),
                    'submenus' => [
                        'index.php' => [
                            'show' => true
                        ],
                        'update-core.php' => [
                            'show' => true
                        ]
                    ]
                ],
                # Posts
                'edit.php' => [
                    'show' => config('menu.posts', false),
                    'submenus' => [
                        'post-new.php' => [
                            'show' => false
                        ],
                        'edit-tags.php?taxonomy=category' => [
                            'show' => false
                        ],
                        'edit-tags.php?taxonomy=post_tag' => [
                            'show' => false
                        ]
                    ]
                ],
                # Media
                'upload.php' => [
                    'show' => config('menu.media', false),
                    'submenus' => [
                        'media-new.php' => [
                            'show' => true
                        ]
                    ]
                ],
                # Pages
                'edit.php?post_type=page' => [
                    'show' => config('menu.pages', false),
                    'submenus' => [
                        'post-new.php?post_type=page' => [
                            'show' => true
                        ]
                    ]
                ],
                # Appearance
                'themes.php' => [
                    'show' => config('menu.appearance', false),
                    'submenus' => [
                        'customize.php?return=' . urlencode($_SERVER['REQUEST_URI']) => [
                            'show' => true
                        ]
                    ]
                ],
                # Plugins
                'plugins.php' => [
                    'show' => config('menu.plugins', false),
                    'submenus' => [
                        'plugin-install.php' => [
                            'show' => false,
                        ],
                        'plugin-editor.php' => [
                            'show' => false,

                        ],
                    ]
                ],
                # Users
                'users.php' => [
                    'show' => config('menu.users', false),
                    'submenus' => [
                        'user-new.php' => [
                            'show' => true,
                        ],
                        'profile.php' => [
                            'show' => true,
                        ],
                    ]
                ],
                # Tools
                'tools.php' => [
                    'show' => config('menu.tools', false),
                    'submenus' => [
                        'import.php' => [
                            'show' => true
                        ],
                        'export.php' => [
                            'show' => true
                        ],
                        'site-health.php' => [
                            'show' => true
                        ],
                        'export-personal-data.php' => [
                            'show' => true
                        ],
                        'erase-personal-data.php' => [
                            'show' => true
                        ]
                    ]
                ],
                # Settings
                'options-general.php' => [
                    'show' => config('menu.settings', false),
                    'submenus' => [
                        'options-writing.php' => [
                            'show' => true
                        ],
                        'options-reading.php' => [
                            'show' => true
                        ],
                        'options-media.php' => [
                            'show' => false
                        ],
                        'options-permalink.php' => [
                            'show' => true
                        ],
                        'options-privacy.php' => [
                            'show' => true
                        ]
                    ]
                ],
            ];

            foreach ($menus as $menu_slug => $menuSettings) {
                if (!$menuSettings['show']) {
                    remove_menu_page($menu_slug);
                }

                if ($menuSettings['show'] && isset($menuSettings['submenus']) && count($menuSettings['submenus']) > 0) {
                    foreach ($menuSettings['submenus'] as $submenuSlug => $submenuSettings) {
                        if (!$submenuSettings['show']) {
                            remove_submenu_page($menu_slug, $submenuSlug);
                        }
                    }
                }


            }
        });
    }
}
