<?php

use Kolon\Core\Container;
use Illuminate\Support\Str;
use Illuminate\Support\HtmlString;

/**
 * Get the container.
 *
 * @param string $abstract
 * @param array $parameters
 * @param Container $container
 *
 * @return Container|mixed
 */
function kolon($abstract = null, $parameters = [], Container $container = null)
{
    $container = $container ?: Container::getInstance();
    if (!$abstract) {
        return $container;
    }

    return $container->bound($abstract)
        ? $container->makeWith($abstract, $parameters)
        : $container->makeWith("kolon.{$abstract}", $parameters);
}

/**
 * Get / set the specified configuration value.
 *
 * If an array is passed as the key, we will assume you want to set an array of values.
 *
 * @param  array|string  $key
 * @param  mixed  $default
 * @return mixed
 */
function config($key = null, $default = null)
{
    if (is_null($key)) {
        return kolon('config');
    }
    if (is_array($key)) {
        return kolon('config')->set($key);
    }

    return kolon('config')->get($key, $default);
}

/**
 * @param string $file
 * @param array $data
 *
 * @return string
 */
function template($file, $data = [])
{
    return kolon('blade')->render($file, $data);
}

/**
 * Retrieve path to a compiled blade view
 *
 * @param $file
 * @param array $data
 *
 * @return string
 */
function template_path($file, $data = [])
{
    return kolon('blade')->compiledPath($file, $data);
}

/**
 * @param string|string[] $templates Possible template files
 *
 * @return array
 */
function filter_templates($templates)
{
    $paths = apply_filters('kolon/filter_templates/paths', [
        'views',
        'resources/views'
    ]);
    $paths_pattern = "#^(" . implode('|', $paths) . ")/#";

    return collect($templates)
        ->map(function ($template) use ($paths_pattern) {
            /** Remove .blade.php/.blade/.php from template names */
            $template = preg_replace('#\.(blade\.?)?(php)?$#', '', ltrim($template));
            /** Remove partial $paths from the beginning of template names */
            if (strpos($template, '/')) {
                $template = preg_replace($paths_pattern, '', $template);
            }

            return $template;
        })
        ->flatMap(function ($template) use ($paths) {
            return collect($paths)
                ->flatMap(function ($path) use ($template) {
                    return [
                        "{$path}/{$template}.blade.php",
                        "{$path}/{$template}.php",
                    ];
                })
                ->concat([
                    "{$template}.blade.php",
                    "{$template}.php",
                ]);
        })
        ->filter()
        ->unique()
        ->all();
}

function mix($path, $manifestDirectory = '')
{
    static $manifests = [];

    $publicFolder = '/public';
    $rootPath = get_theme_file_path();

    $manifestDirectory = $publicFolder . "{$manifestDirectory}";

    if (!Str::startsWith($path, '/')) {
        $path = "/{$path}";
    }

    if (is_file($rootPath . $manifestDirectory . '/hot')) {
        $url = rtrim(file_get_contents($rootPath . $manifestDirectory . '/hot'));


        if (Str::startsWith($url, ['http://', 'https://'])) {
            return new HtmlString(substr(Str::after($url, ':'), 0, -1) . $path);
        }

        return new HtmlString("//localhost:8080{$path}");
    }

    if (!$manifests) {
        if (!file_exists($manifestPath = ($rootPath . $manifestDirectory . '/mix-manifest.json'))) {
            throw new \Exception('The Mix manifest does not exist. ' . $manifestPath);
        }
        $manifests = json_decode(file_get_contents($manifestPath), true);
    }



    if (!array_key_exists($path, $manifests)) {
        throw new \Exception(
            "Unable to locate Mix file: {$path}. Please check your " .
            'webpack.mix.js output paths and try again.'
        );
    }

    return get_theme_file_uri() . $publicFolder . $manifests[$path];
}

function public_path($path = '')
{
    if (!Str::startsWith($path, '/')) {
        $path = "/{$path}";
    }
    return get_theme_file_uri() . '/public' . $path;
}

function getCategories($taxonomy, $options = [])
{
    $categories = get_categories(array_merge([
        'taxonomy' => $taxonomy,
        'hide_empty' => false
    ], $options));

    $items = array_map(function ($item) {
        return (object)[
            'id' => $item->term_id,
            'name' => $item->name,
            'parent_id' => (int)$item->category_parent,
            'children' => []
        ];
    }, $categories);

    return buildTree($items);
}

function getMenuItemsFromLocation($location)
{
    $theme_locations = get_nav_menu_locations();
    if (!isset($theme_locations[$location])) {
        return [];
    }

    $menu_obj = get_term($theme_locations[$location], 'nav_menu');

    $items = wp_get_nav_menu_items($menu_obj);

    $items = array_map(function ($item) {
        return [
            'id' => $item->ID,
            'name' => $item->title,
            'url' => str_replace(home_url(), '', $item->url),
            'parent_id' => (int)$item->menu_item_parent,
            'children' => []
        ];
    }, $items);

    return buildTree($items);
}

function buildTree($elements, $parentId = 0)
{
    $branch = [];

    foreach ($elements as $element) {
        if ($element['parent_id'] == $parentId) {
            $children = buildTree($elements, $element['id']);
            if ($children) {
                $element['children'] = $children;
            }
            $branch[] = $element;
        }
    }

    return $branch;
}
